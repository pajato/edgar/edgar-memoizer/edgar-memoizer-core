package com.pajato.edgar.memoizer.core.launcher

internal interface Launchable {
    fun launch(vararg args: String)
}
