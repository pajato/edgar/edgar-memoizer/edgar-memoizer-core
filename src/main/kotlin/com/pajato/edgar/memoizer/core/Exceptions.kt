package com.pajato.edgar.memoizer.core

class LaunchFailEmptyCommandException : Exception()
class LaunchFailStartError(message: String) : Exception(message)
