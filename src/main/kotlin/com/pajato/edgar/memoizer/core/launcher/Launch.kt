package com.pajato.edgar.memoizer.core.launcher

import com.pajato.edgar.logger.Log
import com.pajato.edgar.memoizer.core.Config
import com.pajato.edgar.memoizer.core.Config.launcher
import com.pajato.edgar.memoizer.core.Config.registerLauncher
import com.pajato.edgar.memoizer.core.savePid

/**
 * Launches the memoizer process. The installed launcher may throw LaunchFail* exceptions.
 */
fun launch(vararg args: String) {
    if (Config.registeredLauncher.size != 1) registerLauncher(Launcher(defaultCommand))
    Log.add("Launcher command size: ${Config.registeredLauncher[0].launchCommand.size}")
    Log.add("Launcher command: ${Config.registeredLauncher[0].launchCommand.joinToString(" ")}")
    launcher.apply { this.launch(*args).also { savePid(process.pid()) } }
}
