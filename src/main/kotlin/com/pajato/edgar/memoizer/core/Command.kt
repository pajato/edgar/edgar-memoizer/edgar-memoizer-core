package com.pajato.edgar.memoizer.core

import kotlinx.serialization.Serializable

interface Command {
    val clientId: String
    val commandName: String
}

@Serializable
class MemoizerCommand(override val clientId: String, override val commandName: String) : Command
