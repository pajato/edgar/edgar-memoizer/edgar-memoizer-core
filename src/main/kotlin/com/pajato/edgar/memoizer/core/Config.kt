package com.pajato.edgar.memoizer.core

import com.pajato.edgar.memoizer.core.launcher.Launcher
import java.io.File
import java.io.RandomAccessFile

/**
 * The default file system location for the memoizer configuration: ~/.config/edgar/memoizer
 */
val defaultConfigDir = File("${System.getProperty("user.home")}/.config/edgar/memoizer")

/**
 * A singleton that provides the Edgar memoizer process configuration.
 */
object Config {
    var configBaseDir: File
        get() = backingConfigBaseDir
        set(dir) { backingConfigBaseDir = dir }

    private var backingConfigBaseDir: File = defaultConfigDir

    val pidFile: RandomAccessFile
        get() = RandomAccessFile(getPidFile(), "rwd")
    val commandsReceiveFile: RandomAccessFile
        get() = RandomAccessFile(getCommandsFile(), "rwd")
    val commandsSendFile: RandomAccessFile
        get() = RandomAccessFile(getCommandsFile(), "rwd")

    val launcher: Launcher
        get() = registeredLauncher[0]

    internal val registeredLauncher: MutableList<Launcher> = mutableListOf()

    internal val logDir: File
        get() = File(configBaseDir, "logs").also { file -> file.mkdirs() }

    /**
     * Register a launcher to be used in starting the memoizer process (app).
     */
    fun registerLauncher(launcher: Launcher) { with(registeredLauncher) { clear(); add(launcher) } }

    internal fun getPidFile() = getCreatedFile("pid")

    private fun getCommandsFile() = getCreatedFile("commands").apply { deleteOnExit() }

    private fun getCreatedFile(name: String) = File(configBaseDir, name).also { if (!it.exists()) it.createNewFile() }
}
