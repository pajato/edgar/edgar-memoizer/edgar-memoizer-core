package com.pajato.edgar.memoizer.core.launcher

import com.pajato.edgar.logger.Log
import com.pajato.edgar.memoizer.core.Config
import com.pajato.edgar.memoizer.core.LaunchFailEmptyCommandException
import com.pajato.edgar.memoizer.core.LaunchFailStartError
import java.io.File

internal const val JarFileName = "app.jar"
internal const val NoReasonMessage = "No reason provided."

internal val defaultCommand = "java -jar ${File(Config.configBaseDir, JarFileName).absolutePath}".split(" ")

internal fun getStartError(exc: Exception): Exception = LaunchFailStartError(exc.message ?: NoReasonMessage)

/**
 * Provide an object which can start a memoizer process (app).
 *
 * @throws LaunchFailStartError when the process cannot be started.
 * @throws LaunchFailEmptyCommandException when the passed in command is empty.
 */
class Launcher(val launchCommand: List<String> = defaultCommand) : Launchable {
    lateinit var process: Process
    fun isProcessInitialized(): Boolean = ::process.isInitialized

    override fun launch(vararg args: String) {
        val startCommand: List<String> = if (args.isEmpty()) launchCommand else getList(*args)

        fun startMemoizer() = ProcessBuilder(startCommand).start()

        fun startProcess() { process = try { startMemoizer() } catch (exc: Exception) { throw getStartError(exc) } }

        if (startCommand.isNotEmpty()) startProcess() else throw LaunchFailEmptyCommandException()
        Log.add("Launched PID: ${process.pid()} using command: ${startCommand.joinToString(" ")}")
    }

    private fun getList(vararg args: String): List<String> = mutableListOf<String>().also {
        it.addAll(launchCommand)
        if (args.isNotEmpty()) it.addAll(args)
    }
}
