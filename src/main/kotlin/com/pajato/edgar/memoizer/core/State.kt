package com.pajato.edgar.memoizer.core

internal interface Pid {
    fun getPid(): Long
}

internal enum class State : Pid {
    Running { override fun getPid(): Long = ProcessHandle.current().pid() },
    Exited { override fun getPid(): Long = -1L },
    Aborted { override fun getPid(): Long = -2L },
    Killed { override fun getPid(): Long = -3L },
    Stale { override fun getPid(): Long = -4L },
}
