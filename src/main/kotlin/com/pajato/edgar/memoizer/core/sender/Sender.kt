package com.pajato.edgar.memoizer.core.sender

import com.pajato.edgar.memoizer.core.MemoizerCommand
import java.nio.ByteBuffer
import java.nio.channels.AsynchronousCloseException
import java.nio.channels.ClosedByInterruptException
import java.nio.channels.ClosedChannelException
import java.nio.channels.FileChannel
import java.nio.channels.FileLock
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

/**
 * Helper function used to support the silent installation and execution of the memoizer app. It is called by the
 * Sender module (aka the Main partition) as a pass-through with the promise that the memoizer app is running and will
 * accept and execute the given command.
 *
 * @throws SendClientNameException when the command client name is the empty string.
 * @throws SendCommandNameException when the command has an empty command name string.
 * @throws WriteFailInterruptedException when the send operation fails because the thread was interrupted.
 * @throws WriteFailAsyncClosedException when the channel was asynchronously closed.
 * @throws WriteFailClosedChannelException when the channel was closed unexpectedly.
 * @throws WriteFailIOErrorException when an I/O error occurred.
 */
fun sendCommandToRunningApp(command: MemoizerCommand, channel: FileChannel) {

    fun getWriteException(exc: Exception): Exception = when (exc) {
        is ClosedByInterruptException -> WriteFailInterruptedException()
        is AsynchronousCloseException -> WriteFailAsyncClosedException()
        is ClosedChannelException -> WriteFailClosedChannelException()
        else /* IOException */ -> WriteFailIOErrorException()
    }

    fun sendCommandAsJson(json: String) {
        fun writeBuffer() { channel.write(ByteBuffer.wrap(json.toByteArray())) }

        fun tryToWriteCommand(lock: FileLock) =
            try { writeBuffer() } catch (exc: Exception) { throw getWriteException(exc) } finally { lock.release() }

        tryToWriteCommand(getExclusiveLock(channel))
    }

    when {
        command.clientId.isEmpty() -> throw SendClientNameException()
        command.commandName.isEmpty() -> throw SendCommandNameException()
        else -> sendCommandAsJson("${Json.encodeToString(command)}\n")
    }
}
