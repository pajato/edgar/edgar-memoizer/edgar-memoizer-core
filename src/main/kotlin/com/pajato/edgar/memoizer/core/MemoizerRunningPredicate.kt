package com.pajato.edgar.memoizer.core

/**
 * A predicate to determine if the memoizer app is running.
 *
 * @return true if and only if the memoizer app is running.
 */
fun memoizerIsRunning(): Boolean = ProcessHandle.of(getMemoizerPid()).let { it.isPresent && it.get().isAlive }

/**
 * A predicate to determine if the memoizer app is not running.
 *
 * @return true if and only if the memoizer app is not running.
 */
fun memoizerIsNotRunning(): Boolean = ProcessHandle.of(getMemoizerPid()).let { !it.isPresent || !it.get().isAlive }
