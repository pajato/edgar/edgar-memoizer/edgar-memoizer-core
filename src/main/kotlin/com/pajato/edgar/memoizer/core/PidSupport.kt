package com.pajato.edgar.memoizer.core

import java.io.RandomAccessFile

/**
 * @return the (long) value stored in the PID configuration file.
 */
fun getMemoizerPid() = RandomAccessFile(Config.getPidFile(), "r").toLong()

internal fun getPidOrErrorCode(pid: Long): Long = when {
    pid > 0 -> pid
    pid == 0L -> -2L
    else -> -3L
}

internal fun RandomAccessFile.toLong() = try { getPidOrErrorCode(readLong()) } catch (exc: Exception) { -1L }

internal fun savePid(pid: Long) = with(Config.pidFile) {
    seek(0)
    writeLong(pid)
}
