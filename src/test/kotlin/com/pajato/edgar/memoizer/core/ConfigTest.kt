package com.pajato.edgar.memoizer.core

import java.io.File
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class ConfigTest : BaseTest() {

    @Test fun `When setting the configuration base dir, verify the log dir`() {
        val dir = testConfigDir
        assertEquals(File(dir, "logs").absolutePath, Config.logDir.absolutePath)
    }

    @Test fun `When accessing the Memoizer pid file, verify behavior`() {
        Config.pidFile.also { file -> assertTrue(file.length() == 0L || file.length() == 4L) }
    }

    @Test fun `When accessing the Memoizer commands receive file, verify behavior`() {
        Config.commandsReceiveFile.also { file -> assertTrue(file.length() == 0L) }
    }

    @Test fun `When accessing the Memoizer commands send file, verify behavior`() {
        Config.commandsSendFile.also { file -> assertTrue(file.length() == 0L) }
    }
}
