package com.pajato.edgar.memoizer.core

import com.pajato.edgar.memoizer.core.ThrowDiscriminant.ClosedChannel
import com.pajato.edgar.memoizer.core.ThrowDiscriminant.NonWritableChannel
import com.pajato.edgar.memoizer.core.ThrowDiscriminant.OverlappingFileLock
import com.pajato.edgar.memoizer.core.sender.LockAlreadyHeldException
import com.pajato.edgar.memoizer.core.sender.LockFailChannelClosedException
import com.pajato.edgar.memoizer.core.sender.LockFailIOErrorException
import com.pajato.edgar.memoizer.core.sender.LockFailOverlappingException
import com.pajato.edgar.memoizer.core.sender.LockFailUnexpected
import com.pajato.edgar.memoizer.core.sender.getExclusiveLock
import kotlin.test.Test
import kotlin.test.assertFailsWith

class CoreTest : BaseTest() {

    @Test fun `When a closed channel exception occurs obtaining a lock, verify diagnostic`() {
        assertFailsWith<LockFailChannelClosedException> { getExclusiveLock(LockFailFileChannel(ClosedChannel)) }
    }

    @Test fun `When an overlapping file lock exception occurs obtaining a lock, verify diagnostic`() {
        assertFailsWith<LockFailOverlappingException> { getExclusiveLock(LockFailFileChannel(OverlappingFileLock)) }
    }

    @Test fun `When an unspecified IO exception occurs obtaining a lock, verify diagnostic`() {
        assertFailsWith<LockFailIOErrorException> { getExclusiveLock(LockFailFileChannel(ThrowDiscriminant.IOError)) }
    }

    @Test fun `When an unexpected exception occurs obtaining a lock, verify diagnostic`() {
        assertFailsWith<LockFailUnexpected> { getExclusiveLock(LockFailFileChannel(NonWritableChannel)) }
    }

    @Test fun `When a successful try lock operations occurs, verify the result`() {
        assertFailsWith<LockAlreadyHeldException> { getExclusiveLock(NullLockFileChannel()) }
    }
}
