package com.pajato.edgar.memoizer.core.sender

import com.pajato.edgar.memoizer.core.BaseTest
import com.pajato.edgar.memoizer.core.Config
import com.pajato.edgar.memoizer.core.LockFailFileChannel
import com.pajato.edgar.memoizer.core.MemoizerCommand
import com.pajato.edgar.memoizer.core.ThrowDiscriminant
import com.pajato.edgar.memoizer.core.WriteFailFileChannel
import kotlin.test.Test
import kotlin.test.assertFailsWith
import org.junit.jupiter.api.TestInfo

class SenderTest : BaseTest() {
    private val name = "exit"
    private val channel = Config.commandsSendFile.channel

    @Test fun `When the client id is empty, verify exception`() {
        val command = MemoizerCommand("", name)
        assertFailsWith<SendClientNameException> { sendCommandToRunningApp(command, channel) }
    }

    @Test fun `When the command name is empty, verify exception`() {
        val command = MemoizerCommand("test", "")
        assertFailsWith<SendCommandNameException> { sendCommandToRunningApp(command, channel) }
    }

    @Test fun `When a send command locking operation fails with a closed by interrupt exception, verify exception`() {
        val id = ThrowDiscriminant.ClosedByInterrupt
        val command = MemoizerCommand(id.name, name)
        assertFailsWith<LockFailInterruptedException> { sendCommandToRunningApp(command, LockFailFileChannel(id)) }
    }

    @Test fun `When a send command locking operation fails with an asynchronous close exception, verify exception`() {
        val id = ThrowDiscriminant.AsynchronousClose
        val command = MemoizerCommand(id.name, name)
        assertFailsWith<LockFailAsyncClosedException> { sendCommandToRunningApp(command, LockFailFileChannel(id)) }
    }

    @Test fun `When a send command operation fails with a closed channel exception, verify exception`() {
        val id = ThrowDiscriminant.ClosedChannel
        val command = MemoizerCommand(id.name, name)
        assertFailsWith<LockFailChannelClosedException> { sendCommandToRunningApp(command, LockFailFileChannel(id)) }
    }

    @Test fun `When a send command operation fails with a unspecified IO error, verify exception`() {
        val id = ThrowDiscriminant.IOError
        val command = MemoizerCommand(id.name, name)
        assertFailsWith<LockFailIOErrorException> { sendCommandToRunningApp(command, LockFailFileChannel(id)) }
    }

    @Test fun `When a send command fails because of a thread interrupt, verify exception`(testInfo: TestInfo) {
        val command = MemoizerCommand(testInfo.displayName, "nop")
        val channel = WriteFailFileChannel(ThrowDiscriminant.ClosedByInterrupt)
        assertFailsWith<WriteFailInterruptedException> { sendCommandToRunningApp(command, channel) }
    }

    @Test fun `When a send fails because of an asynchronous close, verify exception`(testInfo: TestInfo) {
        val command = MemoizerCommand(testInfo.displayName, "nop")
        val channel = WriteFailFileChannel(ThrowDiscriminant.AsynchronousClose)
        assertFailsWith<WriteFailAsyncClosedException> { sendCommandToRunningApp(command, channel) }
    }

    @Test fun `When a send fails because another thread closes the channel, verify exception`(testInfo: TestInfo) {
        val command = MemoizerCommand(testInfo.displayName, "nop")
        val channel = WriteFailFileChannel(ThrowDiscriminant.ClosedChannel)
        assertFailsWith<WriteFailClosedChannelException> { sendCommandToRunningApp(command, channel) }
    }

    @Test fun `When a send fails because of an unspecified IO error, verify exception`(testInfo: TestInfo) {
        val command = MemoizerCommand(testInfo.displayName, "nop")
        val channel = WriteFailFileChannel(ThrowDiscriminant.IOError)
        assertFailsWith<WriteFailIOErrorException> { sendCommandToRunningApp(command, channel) }
    }

    @Test fun `When a normal send command is executed, verify the result`() {
        sendCommandToRunningApp(MemoizerCommand("test()", "nop"), channel)
    }
}
