package com.pajato.edgar.memoizer.core

import com.pajato.edgar.logger.Log
import java.io.File
import java.time.Instant
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.fail
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.TestInfo

val testStartTimeMap: MutableMap<String, Long> = mutableMapOf()
val testConfigDir = File("${System.getProperty("user.dir")}/build/memoizer").also { it.mkdirs() }

abstract class BaseTest(private val configDir: File = testConfigDir) {
    private val testLogDir = File(configDir, "logs")

    @BeforeTest
    fun setUp(info: TestInfo) {
        val id = "${info.testClass.get().name}:${info.displayName}"

        fun initLogs() {
            Log.start(testLogDir)
            Log.add("Starting test $id")
        }

        fun initMemoizer() {
            val currentPid = ProcessHandle.current().pid()
            val memoizerPid = getMemoizerPid()

            fun killAndConfirmDeath(pid: Long) {
                val command = "kill -9 $pid"
                var timeout = 5000L

                suspend fun confirmDeath(): Boolean {
                    fun processIsAlive(pid: Long) = when {
                        pid < 0 || ProcessHandle.of(pid).isEmpty -> false
                        else -> ProcessHandle.of(pid).get().isAlive
                    }

                    while (processIsAlive(pid) && timeout-- != 0L) { delay(1L) }
                    return timeout > 0
                }

                Runtime.getRuntime().exec(command)
                runBlocking { if (!confirmDeath()) fail("Could not kill the memoizer process!") }
                savePid(State.Killed.getPid())
            }

            fun isAlive(pid: Long): Boolean = ProcessHandle.of(pid).let { it.isPresent && it.get().isAlive }

            fun memoizerIsRunning(): Boolean = memoizerPid > 0L && memoizerPid != currentPid && isAlive(memoizerPid)

            when {
                memoizerIsRunning() -> killAndConfirmDeath(memoizerPid)
                memoizerPid > 0 -> savePid(State.Stale.getPid())
                else -> return
            }
        }

        testStartTimeMap[id] = Instant.now().toEpochMilli()
        Config.configBaseDir = configDir
        initLogs()
        initMemoizer()
    }

    @AfterTest
    fun tearDown(info: TestInfo) {
        val id = "${info.testClass.get().name}:${info.displayName}"
        val pid = getMemoizerPid()
        val errorMessage = "Pid is not a negative value, as expected: ($pid)"

        if (pid >= 0) Log.add(errorMessage)
        // assertTrue(pid < 0)
        Log.add("Finished test $id")
        Log.add("Elapsed time (ms): ${Instant.now().toEpochMilli() - testStartTimeMap[id]!!}\n")
    }
}
