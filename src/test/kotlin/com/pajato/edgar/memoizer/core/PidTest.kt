package com.pajato.edgar.memoizer.core

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class PidTest : BaseTest() {

    @Test fun `When saving a pid, verify the result`() {
        val pid = 56932L
        savePid(pid).also {
            assertTrue(Config.pidFile.length() == 8L)
            assertEquals(pid, Config.pidFile.toLong())
        }
        savePid(-1L)
    }

    @Test fun `When the pid file is empty, verify toLong() is correct`() {
        Config.getPidFile().writeText("")
        assertEquals(-1L, Config.pidFile.toLong())
    }

    @Test fun `When the pid file contains a zero value, verify toLong() is correct`() {
        val pid = 0L
        savePid(pid).also { assertEquals(-2L, Config.pidFile.toLong()) }
    }

    @Test fun `When the pid file contains a negative pid, verify toLong() is correct`() {
        val pid = -56932L
        savePid(pid).also { assertEquals(-3L, Config.pidFile.toLong()) }
    }
}
