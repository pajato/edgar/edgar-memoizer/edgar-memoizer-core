package com.pajato.edgar.memoizer.core

import kotlin.test.Test
import kotlin.test.assertEquals

class CommandTest : BaseTest() {

    @Test fun `When creating a Memoizer command, verify the result`() {
        val clientId = "TestId()"
        val commandName = "nop"
        val command = MemoizerCommand(clientId, commandName)
        assertEquals(clientId, command.clientId)
        assertEquals(commandName, command.commandName)
    }
}
