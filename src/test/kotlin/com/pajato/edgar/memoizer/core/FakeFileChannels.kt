package com.pajato.edgar.memoizer.core

import java.io.IOException
import java.nio.ByteBuffer
import java.nio.MappedByteBuffer
import java.nio.channels.AsynchronousCloseException
import java.nio.channels.ClosedByInterruptException
import java.nio.channels.ClosedChannelException
import java.nio.channels.FileChannel
import java.nio.channels.FileLock
import java.nio.channels.NonWritableChannelException
import java.nio.channels.OverlappingFileLockException
import java.nio.channels.ReadableByteChannel
import java.nio.channels.WritableByteChannel

fun getFakeException(discriminant: ThrowDiscriminant): Exception = when (discriminant) {
    ThrowDiscriminant.OverlappingFileLock -> OverlappingFileLockException()
    ThrowDiscriminant.ClosedByInterrupt -> ClosedByInterruptException()
    ThrowDiscriminant.AsynchronousClose -> AsynchronousCloseException()
    ThrowDiscriminant.ClosedChannel -> ClosedChannelException()
    ThrowDiscriminant.IOError -> IOException()
    ThrowDiscriminant.NonWritableChannel -> NonWritableChannelException()
    ThrowDiscriminant.Null -> java.lang.NullPointerException()
}

enum class ThrowDiscriminant {
    OverlappingFileLock, ClosedByInterrupt, AsynchronousClose, ClosedChannel, IOError, NonWritableChannel, Null
}

class LockFailFileChannel(private val discriminant: ThrowDiscriminant) : AbstractFileChannel() {

    override fun lock(position: Long, size: Long, shared: Boolean): FileLock { throw getFakeException(discriminant) }

    override fun tryLock(position: Long, size: Long, shared: Boolean): FileLock { throw getFakeException(discriminant) }
}

class WriteFailFileChannel(private val discriminant: ThrowDiscriminant) : AbstractFileChannel() {

    override fun write(src: ByteBuffer?): Int = throw getFakeException(discriminant)

    override fun lock(position: Long, size: Long, shared: Boolean): FileLock { return TestFileLock() }

    override fun tryLock(position: Long, size: Long, shared: Boolean): FileLock { return TestFileLock() }

    inner class TestFileLock : FileLock(this, 0L, 0L, false) {
        override fun isValid(): Boolean { TODO("Not yet implemented") }

        override fun release() { }
    }
}

class NullLockFileChannel : AbstractFileChannel() {
    override fun lock(position: Long, size: Long, shared: Boolean): FileLock? { return null }

    override fun tryLock(position: Long, size: Long, shared: Boolean): FileLock? { return null }
}

abstract class AbstractFileChannel : FileChannel() {
    private val reason = "Not Implemented"

    override fun implCloseChannel() { TODO(reason) }

    override fun read(dst: ByteBuffer?): Int { TODO(reason) }

    override fun read(dsts: Array<out ByteBuffer>?, offset: Int, length: Int): Long { TODO(reason) }

    override fun read(dst: ByteBuffer?, position: Long): Int { TODO(reason) }

    override fun write(src: ByteBuffer?): Int { TODO(reason) }

    override fun write(srcs: Array<out ByteBuffer>?, offset: Int, length: Int): Long { TODO(reason) }

    override fun write(src: ByteBuffer?, position: Long): Int { TODO(reason) }

    override fun position(): Long { TODO(reason) }

    override fun position(newPosition: Long): FileChannel { TODO(reason) }

    override fun size(): Long { return 1L }

    override fun truncate(size: Long): FileChannel { TODO(reason) }

    override fun force(metaData: Boolean) { TODO(reason) }

    override fun transferTo(position: Long, count: Long, target: WritableByteChannel?): Long { TODO(reason) }

    override fun transferFrom(src: ReadableByteChannel?, position: Long, count: Long): Long { TODO(reason) }

    override fun map(mode: MapMode?, position: Long, size: Long): MappedByteBuffer { TODO(reason) }
}
